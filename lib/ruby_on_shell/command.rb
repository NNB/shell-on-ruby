# frozen_string_literal: true

module ShellOnRuby
  # A command construction, contains the command name and it's arguments / options.
  class Command
    attr_reader :name, :arguments, :options

    def initialize(name, *args, **kargs)
      @name = name.to_s
      @arguments = []
      @options = {}
      command_args = args.flatten.map { |arg| argument_symbol_parse(arg) }
      kargs.map do |key, value|
        if [IO, Array].include?(key.class)
          @options[key] = value
          next
        end

        [argument_symbol_parse(key), value.to_s]
      end.compact.flatten => command_kargs

      @arguments = [*command_args, *command_kargs]
    end

    def self.method_missing(name, *args, **kargs)
      new(name, *args, **kargs)
    end

    private

    def argument_symbol_parse(symbol)
      symbol.is_a?(Symbol) ? "-#{'-' if symbol.size > 1}#{symbol}" : symbol.to_s
    end
  end
end
