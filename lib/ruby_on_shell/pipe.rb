# frozen_string_literal: true

require 'fileutils'

require_relative 'command' unless defined? ShellOnRuby::Command

module ShellOnRuby
  # A shell pipeline.
  class Pipe
    # A standard input for a pipeline.
    class Input
      attr_reader :string

      alias to_s string

      def initialize(string)
        @string = string.to_s
      end

      def method_missing(name, *args, **kargs)
        pipe = ShellOnRuby::Pipe.new(ShellOnRuby::Command.new(name, *args, **kargs))
        pipe.stdin = @string
        pipe
      end
    end

    attr_reader :commands
    attr_accessor :stdin

    def initialize(*commands)
      commands.each do |command|
        next if command.is_a?(ShellOnRuby::Command)

        raise TypeError, "no implicit conversion of #{command.class} into ShellOnRuby::Command"
      end

      @commands = commands
    end

    def self.method_missing(name, *args, **kargs)
      new(ShellOnRuby::Command.new(name, *args, **kargs))
    end

    def method_missing(name, *args, **kargs)
      @commands.append(ShellOnRuby::Command.new(name, *args, **kargs))
      self
    end

    # Execute the pipeline and redirect its output.
    def >(io = nil, append: false, **options)
      case io
      in IO|Array|String
        { out: io }
      in Hash|NilClass
        io || options
      else
        raise TypeError, "no implicit conversion of #{io.class} into IO"
      end.then do |spawn_options|
        spawn_pipeline(@commands, @stdin, spawn_options_parse(spawn_options, append:))
      end
    end

    # Execute the pipeline and redirect its output (append).
    def >>(io = nil, **options)
      self.>(io, append: true, **options)
    end

    private

    def spawn_options_parse(options, append: false)
      options.map do |key, value|
        next [key, value] unless %i[in out err].include?(key) || [Integer, IO, Array].include?(key.class)

        file = [value].flatten(1)
        next [key, value] unless file.first.is_a? String

        file[0] = file.first.sub(/\A~\//, "#{Dir.home}/")
        FileUtils.mkpath(File.dirname(file.first))
        FileUtils.touch(file.first)

        file[1] = append ? 'a' : 'w' unless file[1]

        [key, file]
      end.to_h
    end

    def spawn_pipeline(commands, stdin, options)
      if stdin
        input_read, input_write = IO.pipe
        input_write.puts stdin
        options[:in] = input_read
      end

      wait_threads = []
      last_command_read = nil
      commands.each.with_index do |command, index|
        command_options = options.dup
        command_options[:in] = last_command_read if last_command_read

        if index < commands.size - 1
          command_read, command_write = IO.pipe
          command_options[:out] = command_write
        end

        pid = spawn(command.name, *command.arguments, command.options.merge(command_options))
        wait_threads.append Process.detach(pid)

        [last_command_read, command_write].each { |pipe| pipe&.close }
        last_command_read = command_read
      end

      begin
        wait_threads
      ensure
        input_write&.close
        wait_threads.each(&:join)
      end
    rescue Interrupt => e
      e
    end
  end

  class Command
    def to_pipe
      ShellOnRuby::Pipe.new(self)
    end
  end
end
