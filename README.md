<img width="100%" src="https://capsule-render.vercel.app/api?type=waving&section=header&color=DC2626&fontColor=FEF2F2&height=256&text=Shell%20on%20Ruby&desc=Convenient%20subprocess%20interface&fontAlignY=40" />

## 💡 About

**Shell on Ruby** is a convenient subprocess interface that allows you to call any program as if it were a method.

## 📥 Installation

Install the gem and add to the application's Gemfile by executing:

```sh
bundle add shell_on_ruby
```

If bundler is not being used to manage dependencies, install the gem by executing:

```sh
gem install shell_on_ruby
```

<!--

## ⌨️ Usage

In Ruby do:

```rb
require 'shell_on_ruby'

# Or
require 'shell_on_ruby/sh' # To just require ShellOnRuby::Command and ShellOnRuby::Sh
require 'shell_on_ruby/pipe' # To just require ShellOnRuby::Command and ShellOnRuby::Pipe

# Kernel and string extensions
require 'shell_on_ruby/extensions'

# Or
require 'shell_on_ruby/sh/extensions' # To just require extensions using ShellOnRuby::Sh
require 'shell_on_ruby/pipe/extensions' # To just require extensions using ShellOnRuby::Pipe
```

### `Command`

TODO

### `Sh`

TODO

```rb
ShellOnRuby::Sh.echo('Hello, World!')
```

### `Pipe`

TODO

```rb
ShellOnRuby::Pipe.echo('Hello, World!').figlet(f: 'slant').>

ShellOnRuby::Pipe.echo('fii').tr('i', 'o').figlet > $stderr

ShellOnRuby::Pipe.echo('bar').tr('r', 'z').figlet > "a/b/c/baz.txt"
```

### ➕ Ruby extensions

TODO

```rb
require 'ruby_on_shell/extensions'

# TODO
figlet('Hello, World!', f: 'slant')

# TODO
Sh.ls(Dir.home)

# TODO
'foo'.sh.figlet(f: 'slant')

# TODO
'bar'.sh.tr('r', 'z').sh.figlet

# TODO
Pipe.yes.tr('y', 'n').>

# TODO
'bor'.pipe.tr('o', 'a').figlet > $stderr
```

-->

## 💌 Credits

- [**Python Sh**](https://sh.readthedocs.io) by [Andrew](https://github.com/amoffat)

<a href="https://codeberg.org/NNB">
  <img
    width="100%"
    src="https://capsule-render.vercel.app/api?type=waving&section=footer&color=0284C7&fontColor=F0F9FF&height=128&desc=Made%20with%20%26lt;3%20by%20NNB&descAlignY=80"
    alt="Made with <3 by NNB"
  />
</a>
